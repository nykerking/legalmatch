# LegalMatch

## Usage
### Recommended Usage
	robot -d OUTPUT TEST

# Installation Guide

## Prerequisite

## Windows OS

### Check Admin Rights

1. Press the Windows key (⊞).
1. Type the following:
runas /user:Administrator cmd
1. Hit the Enter key (↵).
1. When prompted for a password, enter your password.
1. The title bar text should be Administrator: Command Prompt not just Command Prompt.

## Mac OS

### Check Admin Rights

1. Run the following command in your Terminal.
sudo ls
1. When prompted for a password, enter your password.
1. A list of directory contents should be displayed.

### Install Command Line Tools

1. Install Command Line Tools by running the following command in your Terminal.
xcode-select --install
1. After the installation, check if Command Line Tools is properly installed by running the following command in your Terminal.
xcode-select --version

### Install Homebrew

1. Install Homebrew by running the following command in your Terminal.
```/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"```
1. After the installation, check if Homebrew is properly installed by running the following command in your Terminal.
```brew doctor```
1. The following message should be displayed.
```Your system is ready to brew.```

### Install Cask

1. Install Cask by running the following command in your Terminal.
```brew install cask```
1. After the installation, check if `cask` is installed by running the following command in your Terminal.
```brew list cask```
1. The version of `cask` should be displayed.

## Robot Framework

## Windows OS

### Install Python 3

1. Download the latest stable Python 3 release from [https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows/).
1. Open the installer.
1. Proceed with the installation.
1. After the installation, close the Command Prompt. Then, reopen it. Check if Python 3 is properly installed by running the following command in your Command Prompt.
```python --version```
1. The downloaded version number of Python 3 should be displayed.

### Check PIP 3 Installation

1. After the installation of Python 3, check if PIP 3 is properly installed by running the following command in your Command Prompt.
```pip --version```
1. The following message should be displayed.
```pip v<X>.<Y> from <file_path>```

### The Virtual Environment

- We will be needing to set up a virtual environment to easily manage and exclude our packages. Create a Virtual Environment

1. Go to your preferred project directory by running the following command in your Command Prompt.
```cd path\to\preferred\directory```
1. In there, run the following command in your Command Prompt.
```python -m venv env```
1. With that, a virtual environment named `env` is now created.
1. Activate the virtual environment by running the following command in your Command Prompt.
```env\Scripts\activate```
1. The start of the next line should be prepended with `(env)`.
1. But be sure to stay the `virtualenv` activated in the succeeding sections.
1. The start of the next line should have `(env)` prepended.

### Install Google Chrome

1. Follow the steps under Install Chrome for Windows in this [link](https://support.google.com/chrome/answer/95346).

### Update Google Chrome

1. Follow the steps in this [link](https://support.google.com/chrome/answer/95414).

### Install ChromeDriver

1. Download the latest stable ChromeDriver in this [link](http://chromedriver.chromium.org/) and choose chromedriver\_win32.zip.
1. Extract the `chromedriver` binary from the zip file.
1. Move the `chromedriver` binary to `<your_preferred_project_directory>\env\Scripts`.
1. To ensure proper installation of `chromedriver`, do the following steps.
  1. Reopen your Command Prompt.
  1. Reactivate your virtual env.
  1. Run the following command in your Command Prompt.
```chromedriver --version```
  1. The version number of `chromedriver` should be displayed.

### Install Python Packages

1. Reopen your Command Prompt.
1. Reactivate your virtual env.
1. Inside the `LEGALMATCH` folder, run the following command in your Command Prompt.
```pip install -r requirements.txt```
1. Wait until the command finishes running.

## Mac OS

### The Built-in Python in Mac

- A system Python 2.7 is already available in your machine but we will be needing Python 3 to future-proof our projects.

### Install Python 3

1. Run the following commands in your Terminal.
```brew install pyenv```
``` brew install zlib```
1. Create or append the following lines to your `~/.bash\_profile`.
```
# For compilers to find zlib you may need to set:
export LDFLAGS="${LDFLAGS} -L/usr/local/opt/zlib/lib"
export CPPFLAGS="${CPPFLAGS} -I/usr/local/opt/zlib/include"
# For pkg-config to find zlib you may need to set:
export PKG_CONFIG_PATH="${PKG_CONFIG_PATH} /usr/local/opt/zlib/lib/pkgconfig"
```
1. Run the following commands in your Terminal.
```eval "$(pyenv init -)"```
1. Go to your preferred project directory by running the following command in your Terminal.
cd /path/to/preferred/directory
1. In there, run the following command in your Terminal.
```pyenv install 3.7.3```
```pyenv local 3.7.3```
```pyenv shell 3.7.3```
```python --version```
1. The following message should be displayed.
```Python 3.<x>.<y>```

### The Virtual Environment

- We will be needing to set up a virtual environment so as not to mess with Mac's built-in system packages.

### Create a Virtual Environment

1. Go to your preferred project directory by running the following command in your Terminal.
```cd /path/to/preferred/directory```
1. In there, run the following command in your Terminal.
```python3 -m venv env```
1. With that, a virtual environment named `env` is now created.
1. Activate the virtual environment by running the following command in your Terminal.
```source env/bin/activate```
1. The start of the next line should be prepended with (env).
1. To deactivate the virtual environment, run the following command in your Terminal.
```deactivate```
1. But be sure to stay the `virtualenv` activated in the succeeding sections.
1. The start of the next line should have `(env)` prepended.

### Install Google Chrome

1. Install Google Chrome by running the following command in your Terminal.
```brew cask install google-chrome```
1. After the installation, check if Google Chrome is installed by opening it.

### Update Google Chrome

1. Follow the steps in this [link](https://support.google.com/chrome/answer/95414).

### Install ChromeDriver

1. Download the latest stable ChromeDriver in this [link](http://chromedriver.chromium.org/) and choose chromedriver\_mac64.zip.
1. Extract the `chromedriver` binary from the zip file.
1. Move the `chromedriver` binary to ```<your_preferred_project_directory>\env\Scripts```
1. To ensure proper installation of `chromedriver`, do the following steps.
  1. Reopen your Command Prompt.
  1. Reactivate your virtual env.
  1. Run the following command in your Command Prompt.
```chromedriver --version```
  1. The version number of `chromedriver` should be displayed.

### Install Python Packages

1. Reopen your Command Prompt.
1. Reactivate your virtual env.
1. Inside the `LEGALMATCH` folder, run the following command in your Command Prompt.
```pip install -r requirements.txt```
1. Wait until the command finishes running.