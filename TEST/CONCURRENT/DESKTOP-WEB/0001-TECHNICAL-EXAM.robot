*** Settings ***
Documentation           Tests the login page objects.
Resource                ${EXECDIR}${/}RESOURCE${/}COMMON.robot
Suite Setup             BUI.RUN KEYWORDS            SELLIB.OPEN BROWSER     about:blank     ${BROWSER}      ${SUT_BROWSER_ALIAS}
...                             AND                 SELLIB.MAXIMIZE BROWSER WINDOW
Suite Teardown          SELLIB.CLOSE BROWSER
Test Teardown           BUI.RUN KEYWORDS            COMMON.WAIT UNTIL GENERIC PAGE FINISHES LOADING
...                             AND                 SELLIB.CAPTURE PAGE SCREENSHOT          ${TEST NAME}.png

*** Keywords ***
DO STEP-08
    [Documentation]     Click "Click here" link
    COMMON.WAIT UNTIL ELEMENT IS INTERACTABLE       ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['CATEGORY']['PSEUDO_LINK_CLICK_HERE']}
    SELLIB.CLICK ELEMENT                            ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['CATEGORY']['PSEUDO_LINK_CLICK_HERE']}
    SELLIB.WAIT UNTIL ELEMENT IS VISIBLE            ${LM_PAGE_HOME['WEB_DESKTOP']['MODAL_OTHER_CATEGORIES']['BUTTON_CLOSE']}

DO STEP-08 TO STEP-12
    [Documentation]     Execute STEP-08 TO STEP-12
    DO STEP-08
    DO STEP-09
    DO STEP-10
    DO STEP-11
    DO STEP-12

DO STEP-09
    [Documentation]     Select a random category and console.log the selected category
    ...                 Sets the selected category text as a suite variable called `new_category`.
    ...                 Returns the selected category text.
    ${webelements_category}=                        SELLIB.GET WEBELEMENTS                  ${LM_PAGE_HOME['WEB_DESKTOP']['MODAL_OTHER_CATEGORIES']['LABELS_TO_TRIM_CATEGORY']}
    ${webelement_to_trim_random_category}=          COMMON.PICK RANDOMLY FROM ARGUMENTS     @{webelements_category}
    COMMON.WAIT UNTIL ELEMENT IS INTERACTABLE       ${webelement_to_trim_random_category}
    ${text_to_trim_random_category}=                SELLIB.GET TEXT                         ${webelement_to_trim_random_category}
    ${text_random_category}=                        STR.STRIP STRING                        ${text_to_trim_random_category}
    BUI.LOG TO CONSOLE                              ${text_random_category}
    BUI.SLEEP           0.25s
    SELLIB.CLICK ELEMENT                            ${webelement_to_trim_random_category}
    BUI.SET SUITE VARIABLE                          ${new_category}                         ${text_random_category}
    [Return]            ${text_random_category}

DO STEP-10
    [Documentation]     Verify page was redericted to ${LM_WEB_URL}/home/caseIntake.do page
    SELLIB.WAIT UNTIL LOCATION IS                   ${LM_WEB_URL}/home/caseIntake.do        timeout=${VISIT_URL_TIMEOUT}

DO STEP-11
    [Documentation]     Verify that "`chosen_category`" string can be found on the "xxx - Most Common Issue" form group label
    [Arguments]         ${chosen_category}=${new_category}
    VERIFY SELECTED CATEGORY IS DISPLAYED AS GROUP LABEL                ${chosen_category}

DO STEP-12
    [Documentation]     Click browser's 'back' button
    SELLIB.GO BACK

VERIFY SELECTED CATEGORY IS DISPLAYED AS GROUP LABEL
    [Documentation]     Fails if `chosen_category` is not found under group label
    [Arguments]         ${chosen_category}=${CATEGORY}
    SELLIB.WAIT UNTIL ELEMENT IS VISIBLE            ${LC_PAGE_CASE_DETAILS['WEB_DESKTOP']['COMMON_ISSUES']['LABEL_LEGEND']}
    BUI.RUN KEYWORD AND CONTINUE ON FAILURE         SELLIB.ELEMENT SHOULD CONTAIN           ${LC_PAGE_CASE_DETAILS['WEB_DESKTOP']['COMMON_ISSUES']['LABEL_LEGEND']}                                 ${chosen_category}

VERIFY TESTIMONIALS ARE NOT THE SAME
    [Documentation]     Fails if `${testimonial_will_be_old}` is the same as `${testimonial_will_be_new}`
    [Arguments]         ${testimonial_will_be_old}
    ${testimonial_will_be_new}=                     SELLIB.GET ELEMENT ATTRIBUTE            ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['LABEL_CONTENT_CURRENT']}                                 data-content
    BUI.SHOULD NOT BE EQUAL AS STRINGS              ${testimonial_will_be_old}              ${testimonial_will_be_new}          msg=Carousel is still animating.

*** Variables ***
${CATEGORY}             Government
${LOCATION}             00001

*** Test Cases ***
STEP-01
    [Documentation]     Open ${LM_WEB_URL}
    SELLIB.GO TO        ${LM_WEB_URL}

STEP-02
    [Documentation]     Select "${CATEGORY}" from "Choose a category" dropdown
    [Setup]             COMMON.WAIT UNTIL ELEMENT IS INTERACTABLE       ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['CATEGORY']['BUTTON_DROPDOWN']}
    # Clicks "Choose a category dropdown"
    SELLIB.CLICK ELEMENT                        ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['CATEGORY']['BUTTON_DROPDOWN']}
    # Clicks "${CATEGORY}" from "Choose a category" dropdown
    SELLIB.WAIT UNTIL ELEMENT IS VISIBLE        ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['CATEGORY']['CONTAINER']}
    SELLIB.CLICK ELEMENT                        ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['CATEGORY']['PSEUDO_OPTION_START']}${CATEGORY}${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['CATEGORY']['PSEUDO_OPTION_END']}      #INLINE_LOCATOR_IS_IN_THIS_LINE

STEP-03
    [Documentation]     Enter "00001" in "ZIP Code or Location" input box and wait for green checkmark icon to appear in the same input box
    [Setup]             COMMON.WAIT UNTIL ELEMENT IS INTERACTABLE       ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['TEXTBOX_LOCATION']}
    SELLIB.INPUT TEXT                           ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['TEXTBOX_LOCATION']}                                      ${LOCATION}
    SELLIB.WAIT UNTIL PAGE CONTAINS ELEMENT     ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['STATE_VALID_LOCATION']}

STEP-04
    [Documentation]     Click "Find a Great Lawyer" button
    [Setup]             COMMON.WAIT UNTIL ELEMENT IS INTERACTABLE       ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['BUTTON_FIND_LAWYER']}
    SELLIB.CLICK ELEMENT                        ${LM_PAGE_HOME['WEB_DESKTOP']['FORM_CASE_INTAKE']['BUTTON_FIND_LAWYER']}

STEP-05
    [Documentation]     Verify page was redericted to ${LM_WEB_URL}/home/caseIntake.do page
    SELLIB.WAIT UNTIL LOCATION IS               ${LM_WEB_URL}/home/caseIntake.do            timeout=${VISIT_URL_TIMEOUT}

STEP-06
    [Documentation]     Verify that "${CATEGORY}" string can be found on the "xxx - Most Common Issue" form group label
    VERIFY SELECTED CATEGORY IS DISPLAYED AS GROUP LABEL                ${CATEGORY}

STEP-07
    [Documentation]     Click browser's 'back' button
    SELLIB.GO BACK

STEP-08
    [Documentation]     Click "Click here" link
    DO STEP-08

STEP-09
    [Documentation]     Select a random category and console.log the selected category
    DO STEP-09

STEP-10
    [Documentation]     Verify page was redericted to ${LM_WEB_URL}/home/caseIntake.do page
    DO STEP-10

STEP-11
    [Documentation]     Verify that "`chosen_category`" string can be found on the "xxx - Most Common Issue" form group label
    DO STEP-11

STEP-12
    [Documentation]     Click browser's 'back' button
    DO STEP-12

STEP-13
    [Documentation]     Repeat STEP-08 to STEP-12 four more times
    BUI.REPEAT KEYWORD      4 times             BUI.RUN KEYWORD AND CONTINUE ON FAILURE     DO STEP-08 TO STEP-12

STEP-14
    [Documentation]     At this point, you should end up back at ${LM_WEB_URL} page. Scroll then to "What People Are Saying About LegalMatch" section
    [Setup]             SELLIB.WAIT UNTIL LOCATION CONTAINS             ${LM_WEB_URL}
    SELLIB.WAIT UNTIL PAGE CONTAINS ELEMENT     ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['LABEL_HEADER']}
    # Scrolling to testimonial header after offsetting the sticky header
    ${header_width}     ${header_height}=       SELLIB.GET ELEMENT SIZE                     ${LM_PAGE_HOME['WEB_DESKTOP']['HEADER']['CONTAINER']}
    ${testimonial_header_y_axis}=               SELLIB.GET VERTICAL POSITION                ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['LABEL_HEADER']}
    ${testimonial_header_y_axis_offset}=        BUI.EVALUATE            ${testimonial_header_y_axis} - ${header_height}
    COMMON.SCROLL PAGE TO LOCATION              ${0}                    ${testimonial_header_y_axis_offset}

STEP-15
    [Documentation]     Click the "right carret button" n+1 times (where n is the number of dots signifying the number of quotes shown on the carousel)
    ${testimonial_1st}=     SELLIB.GET ELEMENT ATTRIBUTE                ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['LABEL_CONTENT_CURRENT']}             data-content
    ${webelements_button_carousel_dot}=         SELLIB.GET WEBELEMENTS                      ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['BUTTONS_CAROUSEL_DOT']}
    FOR         ${webelement_button_carousel_dot}       IN              @{webelements_button_carousel_dot}
                ${testimonial_will_be_old}=     SELLIB.GET ELEMENT ATTRIBUTE                ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['LABEL_CONTENT_CURRENT']}             data-content
                COMMON.WAIT UNTIL ELEMENT IS INTERACTABLE               ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['BUTTON_CAROUSEL_NEXT']}
                BUI.SLEEP   1s
                SELLIB.CLICK ELEMENT                    ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['BUTTON_CAROUSEL_NEXT']}
                BUI.WAIT UNTIL KEYWORD SUCCEEDS         ${ELEMENT_LOAD_TIMEOUT}             1 seconds                           VERIFY TESTIMONIALS ARE NOT THE SAME            ${testimonial_will_be_old}
    END
    BUI.SET SUITE VARIABLE                      ${testimonial_1st}      ${testimonial_1st}

STEP-16
    [Documentation]     Verify that after step 15, you end up back to the first quote
    ${testimonial_current}=                     SELLIB.GET ELEMENT ATTRIBUTE                ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['LABEL_CONTENT_CURRENT']}             data-content
    BUI.RUN KEYWORD AND CONTINUE ON FAILURE     BUI.SHOULD BE EQUAL AS STRINGS              ${testimonial_1st}                  ${testimonial_current}      msg=Current testimonial displayed should be the first testimonial.

STEP-17
    [Documentation]     Repeat step 15 & 16 but this time using "left carret button"
    ${testimonial_1st}=     SELLIB.GET ELEMENT ATTRIBUTE                ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['LABEL_CONTENT_CURRENT']}             data-content
    ${webelements_button_carousel_dot}=         SELLIB.GET WEBELEMENTS                      ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['BUTTONS_CAROUSEL_DOT']}
    FOR         ${webelement_button_carousel_dot}       IN              @{webelements_button_carousel_dot}
                ${testimonial_will_be_old}=     SELLIB.GET ELEMENT ATTRIBUTE                ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['LABEL_CONTENT_CURRENT']}             data-content
                COMMON.WAIT UNTIL ELEMENT IS INTERACTABLE               ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['BUTTON_CAROUSEL_PREV']}
                BUI.SLEEP   1s
                SELLIB.CLICK ELEMENT                    ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['BUTTON_CAROUSEL_PREV']}
                BUI.WAIT UNTIL KEYWORD SUCCEEDS         ${ELEMENT_LOAD_TIMEOUT}             1 seconds                           VERIFY TESTIMONIALS ARE NOT THE SAME            ${testimonial_will_be_old}
    END
    BUI.SET SUITE VARIABLE                      ${testimonial_1st}      ${testimonial_1st}
    ${testimonial_current}=                     SELLIB.GET ELEMENT ATTRIBUTE                ${LM_PAGE_HOME['WEB_DESKTOP']['TESTIMONIALS']['LABEL_CONTENT_CURRENT']}             data-content
    BUI.RUN KEYWORD AND CONTINUE ON FAILURE     BUI.SHOULD BE EQUAL AS STRINGS              ${testimonial_1st}                  ${testimonial_current}      msg=Current testimonial displayed should be the first testimonial.

STEP-18
    [Documentation]     At this point, you should still be at the ${LM_WEB_URL} page. Verify that the string below can be found on the page source
    SELLIB.PAGE SHOULD CONTAIN ELEMENT          css:meta[name='keywords'][content='find a lawyer, find an attorney, find lawyers, find attorneys, legal help']                  #INLINE_LOCATOR_IS_IN_THIS_LINE
