*** Settings ***
Documentation           Global Mapper
Resource                ${EXECDIR}${/}CONFIG${/}LM-WEB-VARIABLE.robot
Resource                ${EXECDIR}${/}CONFIG${/}REQUESTS-CONFIG.robot
Resource                ${EXECDIR}${/}CONFIG${/}SELENIUM-CONFIG.robot
Variables               ${EXECDIR}${/}RESOURCE${/}RESOURCE-MAPPING${/}LM-RESOURCE-MAPPING.py
Library                 BuiltIn             WITH NAME       BUI
Library                 SeleniumLibrary     timeout=${ELEMENT_LOAD_TIMEOUT}     WITH NAME       SELLIB
Library                 String              WITH NAME       STR

*** Keywords ***
GET LOCATOR VALUE FROM STRATEGY-LOCATOR VALUE DICTIONARY
    [Documentation]         Returns the Locator Value from the Strategy-Locator Value.
    [Arguments]             ${strategy_locator_value_dictionary}
    ${strategy}             ${locator_value}=       STR.SPLIT STRING    ${strategy_locator_value_dictionary}    :       max_split=1
    [Return]                ${strategy}     ${locator_value}

PICK RANDOM INDEX FROM ARGUMENTS
    [Documentation]         Returns a random index from the given list
    [Arguments]             @{list}
    ${list_length}=         BUI.GET LENGTH      ${list}
    ${random_index}=        BUI.EVALUATE        random.randint(0, ${list_length}-1)    modules=random
    [Return]                ${random_index}

PICK RANDOMLY FROM ARGUMENTS
    [Documentation]         Returns a random object from the given list.
    [Arguments]             @{list}
    ${random_index}=        COMMON.PICK RANDOM INDEX FROM ARGUMENTS     @{list}
    [Return]                ${list[${random_index}]}

SCROLL PAGE TO LOCATION
    [Documentation]         Scrolls the page to _x_ and _y_.
    [Arguments]             ${x}    ${y}
    SELLIB.EXECUTE JAVASCRIPT       window.scrollTo(${x},${y})

SCROLL TO ELEMENT
    [Documentation]         Sets the screen focus to specific element.
    [Arguments]             ${locator}
    ${strategy}             ${selector}=    COMMON.GET LOCATOR VALUE FROM STRATEGY-LOCATOR VALUE DICTIONARY     ${locator}
    ${query}=               COMMON.USE DOM ELEMENT QUERY   ${strategy}     ${selector}
    SELLIB.EXECUTE JAVASCRIPT               ${query}.scrollIntoView(true)

USE DOM ELEMENT QUERY
    [Documentation]         Returns the JQuery statement based on the ``strategy`` parameter
    [Arguments]             ${strategy}      ${selector}
    ${query}=               BUI.SET VARIABLE IF     "${strategy}"=="id"     document.getElementById('${selector}')
    ...                                     "${strategy}"=="name"   document.getElementsByName('${selector}')[0]    #returns the first DOM element
    ...                                     "${strategy}"=="css"    document.querySelector('${selector}')
    ...                                     "${strategy}"=="xpath"  document.evaluate("${selector}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue
    [Return]                ${query}

WAIT UNTIL ELEMENT IS INTERACTABLE
    [Documentation]         Fails if the element is either invisible or disabled.
    [Arguments]             ${selector}
    SELLIB.WAIT UNTIL ELEMENT IS VISIBLE    ${selector}
    SELLIB.WAIT UNTIL ELEMENT IS ENABLED    ${selector}

WAIT UNTIL GENERIC PAGE FINISHES LOADING
    [Documentation]         Fails if the page did not finish loading after ${PAGE_LOAD_TIMEOUT}.
    SELLIB.WAIT FOR CONDITION       return document.readyState === "complete"       timeout=${PAGE_LOAD_TIMEOUT}