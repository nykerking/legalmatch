# -*- coding: utf-8 -*-

LM_PAGE_HOME = {
	"WEB_DESKTOP": {
		"HEADER": {
			"CONTAINER": "css:.sticky-header"
		},
		"FORM_CASE_INTAKE": {
			"CONTAINER": "id:case-intake-form",
			"CATEGORY": {
				"BUTTON_DROPDOWN": "css:#case-intake-form .dropdown .dropdown-toggle",
				"CONTAINER": "css:#case-intake-form .dropdown.open .dropdown-menu",
				"PSEUDO_OPTION_START": "xpath://*[@id='case-intake-form']//*[contains(@class,'dropdown') and contains(@class, 'open')]//*[contains(@class,'dropdown-menu')]//div[contains(@class, 'dropdown-item') and normalize-space(.)='",
				"PSEUDO_OPTION_END": "']",
				"PSEUDO_TO_TRIM_OPTIONS": "css:#case-intake-form .dropdown.open .dropdown-menu .dropdown-item",
				"PSEUDO_LINK_CLICK_HERE": "xpath://*[@id='case-intake-form']//*[normalize-space(.)='Click here']"
			},
			"TEXTBOX_LOCATION": "css:#case-intake-form .location-input",
			"STATE_VALID_LOCATION": "css:#case-intake-form .case-intake-form__location-checker--valid",
			"BUTTON_FIND_LAWYER": "css:#case-intake-form button[type='submit']"
		},
		"MODAL_OTHER_CATEGORIES": {
			"CONTAINER": "css:.modal-content",
			"BUTTON_CLOSE": "css:.modal.in .modal-body button[aria-label='Close']",
			"LABELS_TO_TRIM_CATEGORY": "css:.modal.in .modal-body .other-categories__item *"
		},
		"TESTIMONIALS": {
			"CONTAINER": "css:.w-testimonials",
			"LABEL_HEADER": "css:.w-testimonials .w-testimonials__header",
			"BUTTON_CAROUSEL_PREV": "css:.w-testimonials .carousel-controls__prev",
			"BUTTON_CAROUSEL_NEXT": "css:.w-testimonials .carousel-controls__next",
			"LABEL_CONTENT_CURRENT": "css:.w-testimonials li:nth-child(2) .w-testimonials__item-body-content",
			"LABEL_AUTHOR_CURRENT": "css:.w-testimonials li:nth-child(2) .w-testimonials__item-body-author",
			"BUTTONS_CAROUSEL_DOT": "css:.w-testimonials .carousel-dots .carousel-dots__dot",
			"BUTTON_CAROUSEL_DOT_CURRENT": "css:.w-testimonials .carousel-dots .carousel-dots__dot--active"
		}
	}
}

LC_PAGE_CASE_DETAILS = {
	"WEB_DESKTOP": {
		"COMMON_ISSUES": {
			"LABEL_LEGEND": "css:#locationForm fieldset.subCategories legend",
		}
	}
}